#!/usr/bin/env python
# -*- coding: latin-1 -*-
from psychopy import visual, event, core, logging, sound, parallel, gui, data
import os, numpy, random
#!/usr/bin/env python
# -*- coding: latin-1 -*-
import win32com, psychopy
from comtypes import *
import comtypes.client
from ctypes import POINTER
from ctypes.wintypes import DWORD, BOOL

MMDeviceApiLib = \
    GUID('{2FDAAFA3-7523-4F66-9957-9D5E7FE698F6}')
IID_IMMDevice = \
    GUID('{D666063F-1587-4E43-81F1-B948E807363F}')
IID_IMMDeviceEnumerator = \
    GUID('{A95664D2-9614-4F35-A746-DE8DB63617E6}')
CLSID_MMDeviceEnumerator = \
    GUID('{BCDE0395-E52F-467C-8E3D-C4579291692E}')
IID_IMMDeviceCollection = \
    GUID('{0BD7A1BE-7A1A-44DB-8397-CC5392387B5E}')
IID_IAudioEndpointVolume = \
    GUID('{5CDF2C82-841E-4546-9722-0CF74078229A}')

class IMMDeviceCollection(IUnknown):
    _iid_ = GUID('{0BD7A1BE-7A1A-44DB-8397-CC5392387B5E}')
    pass

class IAudioEndpointVolume(IUnknown):
    _iid_ = GUID('{5CDF2C82-841E-4546-9722-0CF74078229A}')
    _methods_ = [
        STDMETHOD(HRESULT, 'RegisterControlChangeNotify', []),
        STDMETHOD(HRESULT, 'UnregisterControlChangeNotify', []),
        STDMETHOD(HRESULT, 'GetChannelCount', []),
        COMMETHOD([], HRESULT, 'SetMasterVolumeLevel',
            (['in'], c_float, 'fLevelDB'),
            (['in'], POINTER(GUID), 'pguidEventContext')
        ),
        COMMETHOD([], HRESULT, 'SetMasterVolumeLevelScalar',
            (['in'], c_float, 'fLevelDB'),
            (['in'], POINTER(GUID), 'pguidEventContext')
        ),
        COMMETHOD([], HRESULT, 'GetMasterVolumeLevel',
            (['out','retval'], POINTER(c_float), 'pfLevelDB')
        ),
        COMMETHOD([], HRESULT, 'GetMasterVolumeLevelScalar',
            (['out','retval'], POINTER(c_float), 'pfLevelDB')
        ),
        COMMETHOD([], HRESULT, 'SetChannelVolumeLevel',
            (['in'], DWORD, 'nChannel'),
            (['in'], c_float, 'fLevelDB'),
            (['in'], POINTER(GUID), 'pguidEventContext')
        ),
        COMMETHOD([], HRESULT, 'SetChannelVolumeLevelScalar',
            (['in'], DWORD, 'nChannel'),
            (['in'], c_float, 'fLevelDB'),
            (['in'], POINTER(GUID), 'pguidEventContext')
        ),
        COMMETHOD([], HRESULT, 'GetChannelVolumeLevel',
            (['in'], DWORD, 'nChannel'),
            (['out','retval'], POINTER(c_float), 'pfLevelDB')
        ),
        COMMETHOD([], HRESULT, 'GetChannelVolumeLevelScalar',
            (['in'], DWORD, 'nChannel'),
            (['out','retval'], POINTER(c_float), 'pfLevelDB')
        ),
        COMMETHOD([], HRESULT, 'SetMute',
            (['in'], BOOL, 'bMute'),
            (['in'], POINTER(GUID), 'pguidEventContext')
        ),
        COMMETHOD([], HRESULT, 'GetMute',
            (['out','retval'], POINTER(BOOL), 'pbMute')
        ),
        COMMETHOD([], HRESULT, 'GetVolumeStepInfo',
            (['out','retval'], POINTER(c_float), 'pnStep'),
            (['out','retval'], POINTER(c_float), 'pnStepCount'),
        ),
        COMMETHOD([], HRESULT, 'VolumeStepUp',
            (['in'], POINTER(GUID), 'pguidEventContext')
        ),
        COMMETHOD([], HRESULT, 'VolumeStepDown',
            (['in'], POINTER(GUID), 'pguidEventContext')
        ),
        COMMETHOD([], HRESULT, 'QueryHardwareSupport',
            (['out','retval'], POINTER(DWORD), 'pdwHardwareSupportMask')
        ),
        COMMETHOD([], HRESULT, 'GetVolumeRange',
            (['out','retval'], POINTER(c_float), 'pfMin'),
            (['out','retval'], POINTER(c_float), 'pfMax'),
            (['out','retval'], POINTER(c_float), 'pfIncr')
        ),

    ]

class IMMDevice(IUnknown):
    _iid_ = GUID('{D666063F-1587-4E43-81F1-B948E807363F}')
    _methods_ = [
        COMMETHOD([], HRESULT, 'Activate',
            (['in'], POINTER(GUID), 'iid'),
            (['in'], DWORD, 'dwClsCtx'),
            (['in'], POINTER(DWORD), 'pActivationParans'),
            (['out','retval'], POINTER(POINTER(IAudioEndpointVolume)), 'ppInterface')
        ),
        STDMETHOD(HRESULT, 'OpenPropertyStore', []),
        STDMETHOD(HRESULT, 'GetId', []),
        STDMETHOD(HRESULT, 'GetState', [])
    ]
    pass

class IMMDeviceEnumerator(comtypes.IUnknown):
    _iid_ = GUID('{A95664D2-9614-4F35-A746-DE8DB63617E6}')

    _methods_ = [
        COMMETHOD([], HRESULT, 'EnumAudioEndpoints',
            (['in'], DWORD, 'dataFlow'),
            (['in'], DWORD, 'dwStateMask'),
            (['out','retval'], POINTER(POINTER(IMMDeviceCollection)), 'ppDevices')
        ),
        COMMETHOD([], HRESULT, 'GetDefaultAudioEndpoint',
            (['in'], DWORD, 'dataFlow'),
            (['in'], DWORD, 'role'),
            (['out','retval'], POINTER(POINTER(IMMDevice)), 'ppDevices')
        )
    ]

enumerator = comtypes.CoCreateInstance( 
    CLSID_MMDeviceEnumerator,
    IMMDeviceEnumerator,
    comtypes.CLSCTX_INPROC_SERVER
)

print enumerator
endpoint = enumerator.GetDefaultAudioEndpoint( 0, 1 )
print endpoint
volume = endpoint.Activate( IID_IAudioEndpointVolume, comtypes.CLSCTX_INPROC_SERVER, None )
print volume
print volume.GetMasterVolumeLevel()
print volume.GetVolumeRange()
#volume.SetMasterVolumeLevel(-10.5, None) #10.5 verkar vara mitt i

#Data och fp vars
# Store info about the experiment session
expName = u'Cross-Modality Matching'  #
expInfo = {u'FP':'','session':'001'}

expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
while len(expInfo['FP']) == 0:
    dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
    if dlg.OK == False: core.quit()  # user pressed cancel

dgr = gui.Dlg(title=u"\u00E5lder och k\u00F6n")
dgr.addField(u'\u00E5lder')
dgr.addField(u'K\u00F6n', choices=['Man', 'Kvinna'])
dgr.show()
if dgr.OK == False: core.quit()  # user pressed cancel

#Datafilshantering, skapa data-mappen ifall den ej existerar
if not os.path.isdir('data'):
    os.makedirs('data')
filename = 'data' + os.path.sep + '%s_%s' %(expInfo['FP'], expInfo['expName'])
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)

print type(expInfo['FP'])
#De startnivåer. Den första listan randomiseras den andra tas baklänges
sint = [0.1, 1.0]
vlvl = [5,10,15]
random.shuffle(vlvl)
fpn = int(expInfo['FP'])#Varannan fp far starta med lagsta intensitet pa ljud
if fpn%2==0:
    sint.reverse()

#Skapar fönstret för experimentet
win = visual.Window((1920, 1200), winType='pyglet',
            monitor='testMonitor', units ='deg', screen=0, fullscr=True)
            
#Input --- Mouse + handtagen just nu
myMouse = event.Mouse() #Mus för att respondera
port = parallel.ParallelPort(address=0x1120) #For output
inp = parallel.ParallelPort(address=0x1121) #For input from handles, gulmarkerade = 63, svartmarkerade = 255

#Instruktioner
instruk = u"""Uppgift 1 \u00E4r att matcha ljud med vibrationer. Detta kommer du g\u00F6ra 3 ganger f\u00F6r varje ljud- och vibrationskombination.
    Du ska st\u00E4lla in ljudets volym s\u00E5 att du upplever att vibrationens intensitet matchar ljudvolymen. 

    Den 2:a uppgiften \u00E4r avg\u00F6ra vilken vibration som du upplever \u00E4r lika distraherande som ljudet.

    Tryck p\u00E5 en tangent for att starta"""
avsl = visual.TextStim(win,text=u"Nu \u00E4r experimentet klart. Tack \u00F6r ditt deltagande!", alignHoriz="center", alignVert="center") #Avslutningskärm
instr = visual.TextStim(win,text=instruk, alignHoriz="center", alignVert="center", wrapWidth=40)
event.clearEvents()
instr.draw()
win.flip()
event.waitKeys()
#För responsknapparna att styra ljud och vibrationer med
playStxt = visual.TextStim(win, text="Spela igen", height=0.6, pos=(0,-12))
playS = visual.Rect(win, width=3.2, height=1,  fillColor='darkblue', lineColor=None, pos=(-2,-12) )

#Skapa stim-lista och trials. !!!!!!!!Kanske är det värt att köra 
stimList=[]
for i in range(0,len(vlvl)):
    block = i+1
    for ori in range(1,7):
        if ori <= 3:
            stimList.append( 
                {'lvl':sint[0], 'Trial':ori, 'Block':block,'Vib':vlvl[i]} )
        else: 
            stimList.append( 
                {'lvl':sint[1], 'Trial':ori, 'Block':block, 'Vib':vlvl[i]} )
trials = data.TrialHandler(stimList,1, method="sequential")
trials.data.addDataType('Volym')

snd = 'novel.wav' #för att kunna köra med andra ljud

#Startar experimentet
for thisTrial in trials:
   
    #Bestam vart den ska starta
    if  thisTrial['lvl'] == 0.1:
        mStart = 1 # starta från låg eller hög
    else:
        mStart = 10
    myRatingScale = visual.RatingScale(win, markerStart=mStart, marker="slider", textSize=0.5, high=10, low=1, scale="Lower                                        Higher")
    tada = sound.Sound(snd)
    tx = u"St\u00E4ll in ljudintensitet: " + str(thisTrial['Trial'])
    vb = thisTrial['Vib']

    myItem = visual.TextStim(win, text=tx, height=.12, units='norm')

    fv=sint[0] #GOTTA CHANGE!!!
    event.clearEvents()
    
    while myRatingScale.noResponse: # show & update until a response has been made
        
        for key in event.getKeys():
            if key in ['escape','q']:
                core.quit()
            
        hInp = inp.readData() 
        myItem.draw()
        myRatingScale.draw()
        playS.draw()
        playStxt.draw()
        fv = myRatingScale.getRating()
        fv = float(fv)/10
        win.flip()
        if isinstance(fv, float): #Kan vara överflödigt
            tada.setVolume(fv)   
        else:
            tada.setVolume(fv)

        if playS.contains(myMouse):
            playS.setOpacity(0.1)
        else:
            playS.setOpacity(1)
       
        if myMouse.isPressedIn(playS) or (hInp == 63 or hInp == 255): #Detta gör att man kan starta vibrationerna genom att trycka på handtagen
            port.setData(vb) #Vilken nivå det är i experimentet
            tada.play()
            core.wait(0.2) #Detta gör att duration på vib är 200 ms
            port.setData(0) #Ingen mer vibration tack!
            core.wait(0.8) # Vi kan vänta ett tag tills nästa sak händer

        rating = myRatingScale.getRating() # get the value indicated by the subject, 'None' if skipped 
    trials.data.add('Level', rating )
    trials.data.add('Volym', fv )
    if  not trials.getFutureTrial():
       avsl.draw()
       win.flip()
       event.clearEvents()
       event.waitKeys(maxWait=5)



#Spara
trials.saveAsExcel(fileName=filename, # ...or an xlsx file (which supports sheets)
                  sheetName = 'rawData',
                  stimOut=['Trial', 'Block', 'lvl', 'Vib'], 
                  dataOut=['Level_raw', 'Volym_raw'], appendFile=False)
win.close()
core.quit()

